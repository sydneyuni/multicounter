import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  hideAlert as closeAlert,
  getAlertContent,
  getAlertType,
  isAlertShown,
} from "../features/counter/counterSlice";

const styles = StyleSheet.create({
  background: {
    backgroundColor: "rgba(0,0,0,0.5)",
    position: "absolute",
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  alertBox: {
    backgroundColor: "white",
    width: 250,
    borderRadius: 8,
  },
  text: {
    fontFamily: "SourceSansPro",
    fontSize: 16,
    textAlign: "center",
    paddingVertical: 8,
  },
  textInput: {
    margin: 16,
    borderBottomWidth: 1,
    borderBottomColor: "#80808088",
    fontFamily: "SourceSansPro",
    fontSize: 16,
  },
  title: {
    backgroundColor: "#F0F0F0",
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  mainBody: {
    backgroundColor: "white",
  },
  buttons: {
    flexDirection: "row",
    backgroundColor: "#80808044",
  },
  button: {
    flex: 1,
    backgroundColor: "#F0F0F0",
  },
  buttonMarginRight: {
    marginRight: 2,
  },
  buttonRadiusLeft: {
    borderBottomLeftRadius: 8,
  },
  buttonRadiusRight: {
    borderBottomRightRadius: 8,
  },
});

function AlertDialog() {
  const dispatch = useDispatch();
  const [text, setText] = useState();
  const visible = useSelector(isAlertShown);
  const type = useSelector(getAlertType);
  const {
    title,
    text: contentText,
    success,
    cancel,
  } = useSelector(getAlertContent) || {};
  if (!visible) {
    return null;
  }
  const onButtonPress = (button) => {
    dispatch(closeAlert());
    if (button && button.onPress) {
      if (type === "Text") {
        button.onPress();
      } else {
        button.onPress(text);
      }
    }
  };
  return (
    <View style={styles.background}>
      <View style={styles.alertBox}>
        <Text style={[styles.title, styles.text]}>
          {title || "Are you sure?"}
        </Text>
        <View style={styles.mainBody}>
          {type === "Input" ? (
            <TextInput
              value={contentText}
              onChangeText={setText}
              style={styles.textInput}
            />
          ) : (
            <Text style={styles.text}>{contentText}</Text>
          )}
        </View>
        {type === "Warning" ? (
          <View style={styles.buttons}>
            <TouchableOpacity
              style={[
                styles.button,
                styles.buttonRadiusRight,
                styles.buttonRadiusLeft,
              ]}
              onPress={() => onButtonPress(success)}>
              <Text style={styles.text}>
                {(success && success.text) || "OK"}
              </Text>
            </TouchableOpacity>
          </View>
        ) : (
          <View style={styles.buttons}>
            <TouchableOpacity
              style={[
                styles.button,
                styles.buttonMarginRight,
                styles.buttonRadiusLeft,
              ]}
              onPress={() => onButtonPress(cancel)}>
              <Text style={styles.text}>
                {(cancel && cancel.text) || "Cancel"}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.button, styles.buttonRadiusRight]}
              onPress={() => onButtonPress(success)}>
              <Text style={styles.text}>
                {(success && success.text) || "Yes"}
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    </View>
  );
}

export default AlertDialog;
