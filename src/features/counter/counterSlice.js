import { createSlice } from "@reduxjs/toolkit";

export const counterSlice = createSlice({
  name: "counter",
  initialState: {
    counters: {},
    counterOrder: [],
    showAlert: false,
    alertType: "Text", // 'Text'|'Warning'|'Input'
    alertContent: {},
  },
  reducers: {
    addCounter: (state, action) => {
      const { payload } = action;
      return {
        ...state,
        counters: {
          ...state.counters,
          [payload]: { title: payload, count: 0 },
        },
        counterOrder: [payload, ...state.counterOrder],
      };
    },
    setCounter: (state, action) => {
      const { payload } = action;
      return {
        ...state,
        counters: {
          ...state.counters,
          [payload.title]: {
            ...state.counters[payload.title],
            count: payload.count,
          },
        },
      };
    },
    removeCounter: (state, action) => {
      const { payload } = action;
      return {
        ...state,
        counters: {
          ...state.counters,
          [payload]: undefined,
        },
        counterOrder: state.counterOrder.filter((item) => item !== payload),
      };
    },
    showAlert: (state, action) => {
      const payload = action.payload;
      return {
        ...state,
        showAlert: true,
        alertType: payload.type,
        alertContent: payload.data,
      };
    },
    hideAlert: (state, action) => {
      return {
        ...state,
        showAlert: false,
        alertContent: {},
      };
    },
  },
});

export const { addCounter, setCounter, removeCounter, showAlert, hideAlert } =
  counterSlice.actions;

export const getCounterOrder = (state) => state.counter.counterOrder;
export const getCounters = (state) => state.counter.counters;
export const isAlertShown = (state) => state.counter.showAlert;
export const getAlertType = (state) => state.counter.alertType;
export const getAlertContent = (state) => state.counter.alertContent;

export default counterSlice.reducer;
