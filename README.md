# MultiCounter

MultiCounter is an app that allows users to have multiple counters in parallel that they can increment, decrement, or reset individually.

## Installation

The project is configured using [Expo](https://docs.expo.dev/). You can follow the instructions below to configure and install the application.

1. Clone the project
2. Install dependencies with `npm install` or `yarn install`. This project was mostly configured with Yarn, so if NPM doesn't work you can try Yarn.
3. Run the app with Expo using `yarn ios` (if you haven't used expo before, you may need to [install expo-cli](https://docs.expo.dev/workflow/expo-cli/))

## The assignment

There are two parts of this assignment.

Your first task is to implement the Settings Screen. The settings screen will allow users to toggle the visibility of the add, subtract, and reset button on each counter individually. The setting is immediately applied upon toggle of button states. For example when a button's visibility setting has been disabled, that button wouldn't appear on the home screen. When a button's visibility is enabled, that button will appear on the home screen.

Then, you will also need to send us a video demonstrating of your work and briefly explaining your code. Assume that the audience of your video knows React Native. You don't need any fancy editing or production (a simple screen capture with voiceover is fine), but make sure that your demonstration and code are legible and your voice is clear. The video should be at most 5 minutes long. Please include your first and last name in the file name (e.g. Thomas_Baker_code_demo_USYD.mp4) or hosted video page.

A helpful tip: You may wish to use Quicktime and turn on microphone in the options. Or alternatively try [Loom](https://loom.com).

### Design

The design for this project is in [Figma](https://www.figma.com/file/Rb7xTKvAqZgaYJWEhSoyME/MultiCounter). You should be able to download assets from the Figma design file as well.

The Font used is [Source Sans Pro](https://fonts.google.com/specimen/Source+Sans+Pro). The icons are from [expo-vector-icons](https://docs.expo.dev/guides/icons/#expovector-icons) is used. You shouldn't need to download any additional libraries or font to the base code; they have been pre-configured for you. The base code uses the same font and icon set, so you should be able to find examples on how to use them.

### Submission

To submit your code, fork this repository into your own repository, then make a branch from `develop` and name it `your-name-here/name-of-feature` (e.g. `thomas-baker/delete-counter`). Once you have finished your feature, please make a pull request with the following details:

- Destination : `develop`
- Title : Name of Your Feature in Plain English
- Description :
  - List of changes that you made (For example: Add a home screen to the app)
  - What platform you are able to test on (e.g. IOS, Android, or Web)

To submit your video, send an email to [steve.pascoe@sydney.edu.au](mailto:steve.pascoe@sydney.edu.au) and [daniel.burn@sydney.edu.au](mailto:daniel.burn@sydney.edu.au) with your video attached.

### Criteria

Generally, we will be looking at the following:

- Whether your code works and adhere to the specifications
- How clean your code is
- Your understanding of your own code
- How well you can explain your code

Note: ideally your code runs on both iOS Simulator and Android Emulator. As part of our review your code will be run on the iOS simulator. Please notify us (e.g. by writing in your PR description or by email) if you are unable to test on iOS or have any other issues running the code.

And have fun with it, happy coding!
